"""
URL configuration for exchange project.

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from webapp import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name='index'),
    path('login/', views.login, name='login'),
    path('register/', views.register, name='register'),
    path('logout', views.logout, name='logout'),
    path('crypto_dashboard/', views.crypto_dashboard, name='crypto_dashboard'),
    path('crypto_dashboard2/', views.crypto_dashboard2, name='crypto_dashboard2'),
    path('mywatchlist/', views.mywatchlist, name='mywatchlist'),
    path('crypto_dashboard/detail/<int:id>', views.crypto_detail, name='crypto_detail'),
    path('crypto_dashboard/buy/<int:id>', views.crypto_buy, name='crypto_buy'),
    path('crypto_dashboard/buy/form/<int:id>', views.buy_form, name='buy_form'),
    path('crypto_dashboard/add_to_watchlist/<int:id>', views.add_to_watchlist, name='add_to_watchlist'),
    path('crypto_dashboard/delete_from_watchlist/<int:id>', views.delete_from_watchlist, name='delete_from_watchlist'),
    path('crypto_dashboard/sell/<int:id>', views.crypto_sell, name='crypto_sell'),
    path('crypto_dashboard/sell/form/<int:id>', views.sell_form, name='sell_form'),
    path('home/', views.home, name='home'),
    path('delete_transaction/<int:id>/', views.delete_transaction, name='delete_transaction'),
    path('edit_transaction/<int:id>/', views.edit_transaction, name='edit_transaction'),
    path('edit_transaction_form/<int:id>/', views.edit_transaction_form, name='edit_transaction_form'),
    path('profile/', views.profile, name='profile'),
    path('edit_profile/', views.edit_profile, name='edit_profile'),
    path('edit_profile_form/', views.edit_profile_form, name='edit_profile_form'),
    path('add_cryptocurrency/', views.add_cryptocurrency, name='add_cryptocurrency'),
    path('add_cryptocurrency_form/', views.add_cryptocurrency_form, name='add_cryptocurrency_form'),
    path('add_category/', views.add_category, name='add_category'),
    path('add_category_form/', views.add_category_form, name='add_category_form'),
]
