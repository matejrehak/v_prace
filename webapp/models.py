from django.db import models
from datetime import datetime


class User(models.Model):
    fname = models.CharField(max_length=30)
    lname = models.CharField(max_length=30)
    email = models.CharField(max_length=30)
    password = models.CharField(max_length=30)
    role = models.CharField(max_length=1, default='U')
    objects = models.Manager

    def __str__(self):
        return "{} {}".format(self.fname, self.lname)

class Transaction(models.Model):
    date_transaction = models.DateTimeField(default=datetime.now)
    amount_cryptocurrency = models.FloatField()
    type = models.CharField(max_length=4)
    user = models.ForeignKey('User', on_delete=models.CASCADE)
    cryptocurrency = models.ForeignKey('Cryptocurrency', on_delete=models.CASCADE)
    objects = models.Manager

class Cryptocurrency(models.Model):
    name = models.CharField(max_length=30)
    price = models.FloatField()
    description = models.TextField()
    category = models.ForeignKey('Category', on_delete=models.CASCADE)
    objects = models.Manager

    def __str__(self):
        return self.name

class Cryptocurrency_History(models.Model):
    modified_at = models.DateTimeField()
    old_price = models.FloatField()
    new_price = models.FloatField()
    user = models.ForeignKey('User', on_delete=models.CASCADE)
    cryptocurrency = models.ForeignKey('Cryptocurrency', on_delete=models.CASCADE)
    objects = models.Manager

    def __str__(self):
        return self.cryptocurrency.name

class Category(models.Model):
    name = models.CharField(max_length=30)
    description = models.TextField()
    objects = models.Manager

    def __str__(self):
        return self.name

class Watchlist(models.Model):
    user = models.ForeignKey('User', on_delete=models.CASCADE)
    cryptocurrency = models.ForeignKey('Cryptocurrency', on_delete=models.CASCADE)
    objects = models.Manager

class Payment(models.Model):
    status = models.CharField(max_length=30)
    price_payment = models.FloatField()
    method_payment = models.CharField(max_length=30)
    transaction = models.ForeignKey('Transaction', on_delete=models.CASCADE)
    objects = models.Manager



