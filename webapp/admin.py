from django.contrib import admin
from .models import User, Transaction, Cryptocurrency, Cryptocurrency_History, Category, Watchlist, Payment

# Register your models here.

admin.site.register(User)
admin.site.register(Transaction)
admin.site.register(Cryptocurrency)
admin.site.register(Cryptocurrency_History)
admin.site.register(Category)
admin.site.register(Watchlist)
admin.site.register(Payment)
