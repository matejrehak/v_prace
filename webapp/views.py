import django.http
from django.shortcuts import render, redirect
from django.http import HttpResponse
from webapp.models import User, Transaction, Cryptocurrency, Cryptocurrency_History, Category, Watchlist, Payment
from webapp.forms import LoginForm, RegisterForm, BuyForm, SellForm, EditTransactionForm, EditProfileForm, AddCryptocurrencyForm, AddCategoryForm
from django.shortcuts import get_object_or_404
from django.db.models import F, Sum
from django.http import JsonResponse

# Create your views here.

def index(request):
    users = User.objects.count()
    transactions = Transaction.objects.count()
    cryptocurrencies = Cryptocurrency.objects.count()
    context = {
        'users': users,
        'transactions': transactions,
        'cryptocurencies': cryptocurrencies,
    }
    return render(request, 'index.html', context)

def home(request):
    transactions = Transaction.objects.filter(user=request.session['id'])
    transactions_count = Transaction.objects.filter(user=request.session['id']).count()
    favorite_count = Watchlist.objects.filter(user=request.session['id']).count()
    buy_transactions = (
        Transaction.objects
        .filter(type='Buy',user=request.session['id'])
        .values('cryptocurrency__name')
        .annotate(buy_total=Sum(F('amount_cryptocurrency') * F('cryptocurrency__price')))
    )

    sell_transactions = (
        Transaction.objects
        .filter(type='Sell',user=request.session['id'])
        .values('cryptocurrency__name')
        .annotate(sell_total=Sum(F('amount_cryptocurrency') * F('cryptocurrency__price')))
    )
    totals = {}
    for transaction in buy_transactions:
        totals[transaction['cryptocurrency__name']] = transaction['buy_total']
    for transaction in sell_transactions:
        if transaction['cryptocurrency__name'] in totals:
            totals[transaction['cryptocurrency__name']] -= transaction['sell_total']
        else:
            totals[transaction['cryptocurrency__name']] = -transaction['sell_total']

    final_total = sum(totals.values())

    return render(request, 'home.html', {'transactions':transactions, 'transaction_count':transactions_count, 'favorite_count':favorite_count, 'final_total':final_total})

def logout(request):
    request.session.delete()
    return redirect('index')


def login(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            e = form.cleaned_data['login_email']
            p = form.cleaned_data['login_password']
            if User.objects.filter(email=e).count() == 1:
                user = User.objects.get(email=e)
                if user.password == p:
                    request.session['logged'] = True
                    request.session['login'] = user.email
                    request.session['id'] = user.id
                    request.session['role'] = user.role
                    return redirect('home')
                else:
                    return render(request, 'login.html', {'form': form, 'login_message': 'Invalid password or email'})
            else:
                return render(request, 'login.html', {'form': form, 'login_message': 'Invalid password or email'})
        else:
            return render(request, 'login.html')
    else:
        form = LoginForm()
        return render(request, 'login.html', {'form': form, 'login_message': ''})

def crypto_dashboard(request):
    cryptocurrencies = Cryptocurrency.objects.all()
    user = get_object_or_404(User, id=request.session['id'])
    watchlist_list = Watchlist.objects.filter(user=user)
    list = []
    for item in watchlist_list:
        cryptocurrency = item.cryptocurrency
        list.append(cryptocurrency)
    context = {'cryptocurrencies': cryptocurrencies, 'list':list}
    return render(request, 'crypto_dashboard.html', context)

def crypto_dashboard2(request):
    cryptocurrencies = Cryptocurrency.objects.all()

    context = {'cryptocurrencies': cryptocurrencies}
    return render(request, 'crypto_dashboard.html', context)

def mywatchlist(request):
    watchlist_items = Watchlist.objects.filter(user=request.session['id'])
    cryptocurrencies = []
    for item in watchlist_items:
        cryptocurrency = item.cryptocurrency
        cryptocurrency_history = Cryptocurrency_History.objects.filter(cryptocurrency=cryptocurrency)
        cryptocurrencies.append((cryptocurrency, cryptocurrency_history))

    context = {
        'cryptocurrencies': cryptocurrencies
    }
    return render(request, 'mywatchlist.html', context)

def add_to_watchlist(request, id):
    crypto = get_object_or_404(Cryptocurrency, id=id)
    user = get_object_or_404(User, id=request.session['id'])
    watchlist = Watchlist(cryptocurrency=crypto, user=user)
    watchlist.save()
    return redirect('mywatchlist')

def delete_from_watchlist(request, id):
    crypto = get_object_or_404(Cryptocurrency, id=id)
    user = get_object_or_404(User, id=request.session['id'])
    watchlist = get_object_or_404(Watchlist, cryptocurrency=crypto, user=user)
    watchlist.delete()
    return redirect('crypto_dashboard')

def profile(request):
    user = get_object_or_404(User, id=request.session['id'])
    return render(request, 'profile.html', {'user':user})

def edit_profile(request):
    user = get_object_or_404(User, id=request.session['id'])
    return render(request, 'edit_profile.html', {'user':user})

def crypto_detail(request, id):
    crypto= get_object_or_404(Cryptocurrency,pk=id)
    cryptocurrency_history = Cryptocurrency_History.objects.filter(cryptocurrency=crypto)
    return render(request, 'crypto_detail.html',{'cryptocurrency':crypto, 'cryptocurrency_history':cryptocurrency_history})

def crypto_buy(request, id):
    crypto = get_object_or_404(Cryptocurrency,pk=id)
    return render(request, 'crypto_buy.html',{'cryptocurrency':crypto})

def crypto_sell(request, id):
    crypto = get_object_or_404(Cryptocurrency,pk=id)
    return render(request, 'crypto_sell.html',{'cryptocurrency':crypto})

def buy_form(request, id):
    cryptocurrency = get_object_or_404(Cryptocurrency, id=id)
    if request.method == 'POST':
        form = BuyForm(request.POST)
        if form.is_valid():
            user = get_object_or_404(User, email=request.session['login'])
            crypto = get_object_or_404(Cryptocurrency, name=form.cleaned_data['cryptocurrency'])
            date = form.cleaned_data['date_transaction']
            amount = form.cleaned_data['amount']
            transaction = Transaction(date_transaction=date, amount_cryptocurrency=amount, type='Buy', user=user, cryptocurrency=crypto)
            transaction.save()
            return redirect('home')
        else:
            print(form.errors)
            return render(request, 'crypto_buy.html', {'cryptocurrency':cryptocurrency, 'form': form})
    else:
        form = BuyForm()
        return render(request, 'crypto_buy.html', {'cryptocurrency': cryptocurrency, 'form': form})

def sell_form(request, id):
    cryptocurrency = get_object_or_404(Cryptocurrency, id=id)
    if request.method == 'POST':
        form = SellForm(request.POST)
        if form.is_valid():
            user = get_object_or_404(User, email=request.session['login'])
            crypto = get_object_or_404(Cryptocurrency, name=form.cleaned_data['cryptocurrency'])
            date = form.cleaned_data['date_transaction']
            amount = form.cleaned_data['amount']
            transaction = Transaction(date_transaction=date, amount_cryptocurrency=amount, type='Sell', user=user, cryptocurrency=crypto)
            transaction.save()
            return redirect('home')
        else:
            return render(request, 'crypto_sell.html', {'cryptocurrency':cryptocurrency, 'form': form})
    else:
        form = SellForm()
        return render(request, 'crypto_sell.html', {'cryptocurrency': cryptocurrency, 'form': form})

def register(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            e = form.cleaned_data['register_email']
            if User.objects.filter(email = e).count() == 0:
                fname = form.cleaned_data['register_fname']
                lname = form.cleaned_data['register_lname']
                password = form.cleaned_data['register_password']
                c_password = form.cleaned_data['register_c_password']
                user = User(fname = fname, lname = lname, password = password, email = e)
                user.save()
                request.session['logged'] = True
                request.session['login'] = user.email
                request.session['id'] = user.id
                request.session['role'] = user.role
                return redirect('home')
            else:
                return render(request, 'register.html', {'form': form, 'register_message': 'Account exists'})
        else:
            return redirect('register')
    return render(request, 'register.html', {'register_message': ''})

def delete_transaction(request, id):
    try:
        t = Transaction.objects.get(id = id)
    except Transaction.DoesNotExist:
        raise django.http.Http404("Transaction not found")
    t.delete()
    return redirect('home')


def edit_transaction(request, id):
    transaction = get_object_or_404(Transaction, id=id)
    cryptocurrencies = Cryptocurrency.objects.exclude(id=transaction.cryptocurrency.id)
    return render(request, 'edit_transaction.html', {'transaction':transaction, 'cryptocurrencies':cryptocurrencies})

def edit_transaction_form(request, id):
    transaction = get_object_or_404(Transaction, id=id)
    cryptocurrency = get_object_or_404(Cryptocurrency, id=transaction.cryptocurrency.id)
    if request.method == 'POST':
        form = EditTransactionForm(request.POST)
        if form.is_valid():
            cryptocurrencyNew = get_object_or_404(Cryptocurrency, name=form.cleaned_data['cryptocurrency'])
            transaction.cryptocurrency = cryptocurrencyNew
            transaction.date_transaction = form.cleaned_data['date_transaction']
            transaction.amount_cryptocurrency = form.cleaned_data['amount']
            transaction.save()
            return redirect('home')
        else:
            print(form.errors)
            return render(request, 'edit_transaction.html', {'cryptocurrency':cryptocurrency, 'form': form})
    else:
        form = EditTransactionForm()
        return render(request, 'edit_transaction.html', {'cryptocurrency': cryptocurrency, 'form': form})

def edit_profile_form(request):
    user = get_object_or_404(User, id=request.session['id'])
    if request.method == 'POST':
        form = EditProfileForm(request.POST)
        if form.is_valid():
            user.fname = form.cleaned_data['fname']
            user.lname = form.cleaned_data['lname']
            user.email = form.cleaned_data['email']
            user.save()
            return redirect('profile')
        else:
            print(form.errors)
            return render(request, 'edit_profile.html', {'form': form})
    else:
        form = EditTransactionForm()
        return render(request, 'edit_profile.html', {'form': form})

def add_cryptocurrency(request):
    categories = Category.objects.all()
    return render(request, 'add_cryptocurrency.html', {'categories': categories})

def add_category(request):
    return render(request, 'add_category.html')

def add_cryptocurrency_form(request):
    if request.method == 'POST':
        form = AddCryptocurrencyForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            price = form.cleaned_data['price']
            description = form.cleaned_data['description']
            category = get_object_or_404(Category, name=form.cleaned_data['category'])
            cryptocurrency = Cryptocurrency(name=name, price=price, description=description, category=category)
            cryptocurrency.save()
            return redirect('crypto_dashboard')
        else:
            print(form.errors)
            return render(request, 'add_cryptocurrency.html', {'form': form})
    else:
        form = AddCryptocurrencyForm()
        return render(request, 'add_cryptocurrency.html', {'form': form})

def add_category_form(request):
    if request.method == 'POST':
        form = AddCategoryForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            description = form.cleaned_data['description']
            category = Category(name=name, description=description)
            category.save()
            return redirect('crypto_dashboard')
        else:
            print(form.errors)
            return render(request, 'add_cryptocurrency.html', {'form': form})
    else:
        form = AddCryptocurrencyForm()
        return render(request, 'add_cryptocurrency.html', {'form': form})



