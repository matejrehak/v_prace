from django import forms

from webapp.models import User, Transaction, Cryptocurrency, Cryptocurrency_History, Category, Watchlist, Payment

class LoginForm(forms.Form):
    login_email = forms.CharField(label='Email', max_length=30)
    login_password = forms.CharField(label='Password', max_length=30)

class RegisterForm(forms.Form):
    register_fname = forms.CharField(label='First name', max_length=30)
    register_lname = forms.CharField(label='Last name', max_length=30)
    register_email = forms.CharField(label='Email', max_length=30)
    register_password = forms.CharField(label='Password', max_length=30)
    register_c_password = forms.CharField(label='C_Password', max_length=30)

class BuyForm(forms.Form):
    date_transaction = forms.DateField(label='Date of transaction')
    amount = forms.FloatField(label='Amount')
    payment = forms.CharField(label='Payment', max_length=30)
    cryptocurrency = forms.CharField(label='Cryptocurrency', max_length=30)

class SellForm(forms.Form):
    date_transaction = forms.DateField(label='Date of transaction')
    amount = forms.FloatField(label='Amount')
    cryptocurrency = forms.CharField(label='Cryptocurrency', max_length=30)

class EditTransactionForm(forms.Form):
    date_transaction = forms.DateField(label='Date of transaction')
    amount = forms.FloatField(label='Amount')
    type = forms.CharField(label='Type', max_length=4)
    cryptocurrency = forms.CharField(label='Cryptocurrency', max_length=30)

class EditProfileForm(forms.Form):
    fname = forms.CharField(label='First name', max_length=30)
    lname = forms.CharField(label='Last name', max_length=30)
    email = forms.CharField(label='Email', max_length=30)

class AddCryptocurrencyForm(forms.Form):
    name = forms.CharField(label='Name', max_length=30)
    price = forms.FloatField(label='Price')
    description = forms.CharField(label='Description', max_length=300)
    category = forms.CharField(label='Category', max_length=30)

class AddCategoryForm(forms.Form):
    name = forms.CharField(label='Name', max_length=30)
    description = forms.CharField(label='Description', max_length=300)